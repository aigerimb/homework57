import React, { Component } from 'react';
import './ModalApp.css';
import Modal from "../../components/UI/Modal/Modal";
import { Button } from 'reactstrap';
import Alert from '../../components/UI/Alert/Alert'

class ModalApp extends Component {
  state = {
    isModalShown: false,
    isAlertShown: false,
    continue: false
  }

  buttonClickHandler = () => {
    this.setState({isModalShown: true})
  };

  closeModal = () => {
    this.setState({isModalShown: false})
  };

  continued = () => {
    this.setState({isModalShown: false, continue: true, isAlertShown: true})
  }

  buttonList = [
    {type: 'primary', label: 'Continue', clicked: this.continued},
    {type: 'danger', label: 'Close', clicked: this.closeModal}
  ]

  render() {
    return (
      <div className="ModalApp">
        <Button color="primary" outline onClick={this.buttonClickHandler}>Open Modal</Button>
        <Modal show={this.state.isModalShown} closed={this.closeModal} title="Modal title">
          <p>This is modal content</p>
          {
            this.buttonList.map(button => {
              return (
                <Button
                  key={button.type+button.label}
                  color={button.type}
                  className="ModalButton"
                  onClick={button.clicked}
                >
                  {button.label}
                </Button>
              )
              
            })
          }
        </Modal>
        {
          this.state.continue ? 
          <Alert
            type="warning"
            show={this.state.isAlertShown} 
          >
            You pressed "Continue" on Modal
          </Alert> : null
        }
      </div>
    );
  }
}

export default ModalApp;
