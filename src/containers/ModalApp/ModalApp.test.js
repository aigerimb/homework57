import { render, screen } from '@testing-library/react';
import ModalApp from '../ModalApp/ModalApp';

test('renders learn react link', () => {
  render(<ModalApp />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
