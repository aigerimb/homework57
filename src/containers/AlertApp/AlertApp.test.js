import { render, screen } from '@testing-library/react';
import AlertApp from '../AlertApp/AlertApp';

test('renders learn react link', () => {
  render(<AlertApp />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
