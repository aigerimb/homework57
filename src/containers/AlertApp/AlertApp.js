import React, { Component } from 'react';
import './AlertApp.css';
import Alert from "../../components/UI/Alert/Alert"
import { Button } from 'reactstrap';

class AlertApp extends Component {
  state = {
    isAlertShown: false,
  }

  buttonClickHandler = () => {
    this.setState({isAlertShown: true})
  };

  closeAlert = () => {
    this.setState({isAlertShown: false})
  };

  render() {
    return (
      <div className="AlertApp">
        <Button outline onClick={this.buttonClickHandler}>Open Alert</Button>
        <Alert 
            type="primary"
            show={this.state.isAlertShown} 
            dismiss={this.closeAlert}
        >
          <p>This is Alert content</p>
        </Alert>
      </div>
    );
  }
}

export default AlertApp;
