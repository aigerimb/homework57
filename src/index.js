import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ModalApp from './containers/ModalApp/ModalApp';
import AlertApp from './containers/AlertApp/AlertApp';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <ModalApp />
    <AlertApp />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
