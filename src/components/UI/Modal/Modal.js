import React from 'react';
import './Modal.css';
import Backdrop from './Backdrop/Backdrop';
import { IconButton } from "@mui/material";
import CloseIcon from '@mui/icons-material/Close';


const Modal = props => {
    return (
        <>
            <Backdrop show={props.show} clicked={props.closed} />
            <div 
                className="Modal"
                style={{
                    transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                    opacity: props.show ? '1' : '0'
                }}
            >
                <IconButton aria-label="delete" size="small" className='CloseButton' onClick={props.closed}>
                    <CloseIcon fontSize="small" />
                </IconButton>
                <div className='TitleWrapper'>
                    <h3 className='Title'>{props.title}</h3>
                    <div className='Line'></div>
                </div>
                {props.children}
            </div>
        </>
    )
};

export default Modal;
