import React from 'react';
import './Alert.css';
import { Button } from 'reactstrap';

const types = [
    {name: "primary", bgColor: "#cce5ff", textColor: "#007bff" },
    {name: "success", bgColor: "#d4edda", textColor: "#28a745" },
    {name: "danger", bgColor: "#f8d7da", textColor: "#dc3545" },
    {name: "warning", bgColor: "#fff3cd", textColor: "#ffc107" },
]


const Alert = props => {
    let colorStyle;
    types.forEach(type => {
        if (props.type === type.name) {
            colorStyle = type;
        }
    });

    return (
            <div 
                className="Alert"
                style={{
                    transform: props.show ? 'scale(1)' : 'scale(0)',
                    color: colorStyle.textColor,
                    background: colorStyle.bgColor,
                    borderColor: colorStyle.textColor
                }}
            >
                <Button close className='CloseALertButton' onClick={props.dismiss}
                    style={{
                        display: props.dismiss ? 'block' : 'none'
                    }}
                />
                {props.children}
            </div>
    )
};

export default Alert;
